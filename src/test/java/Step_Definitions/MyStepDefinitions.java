package Step_Definitions;

import Elements.MainPage;
import Elements.MarketPage;
import Hook.Hooks;
import cucumber.api.java.ru.Дано;
import cucumber.api.java.ru.И;
import cucumber.api.java.ru.Когда;
import cucumber.api.java.ru.Тогда;

public class MyStepDefinitions
{
    MainPage MainP = new MainPage();
    MarketPage MarkP = new MarketPage(Hooks.driver);
    public static String Platform;

    @Дано("^Открыть \"([^\"]*)\"$")
    public void открыть(String browser) throws Throwable
    {
        if (browser.contains("chrome"))
        {
            Hooks.chrome();
        }
        else if (browser.contains("firefox"))
        {
            Hooks.firefox();
        }
    }

    @Когда("^Зайти на страницу \"([^\"]*)\"$")
    public void зайти_на_страницу(String page) throws Throwable
    {
        Hooks.driver.navigate().to(page);

        Platform = page.substring(page.indexOf(':') + 3, page.lastIndexOf('.'));
    }
    @И("^Перешли в \"([^\"]*)\"$")
    public void перешлиВ(String page) throws Throwable
    {
        MainP.GoTo(page);
    }

    @И("^Перешли в раздел \"([^\"]*)\"$")
    public void перешли_в_раздел(String page) throws Throwable
    {
         MarkP.GoToSection(page);
    }
    @И("^Перешли в подраздел \"([^\"]*)\"$")
    public void отфильтровали(String type) throws Throwable
    {
        MarkP.GoToSubSection(type);
    }

    @И("^Зашли в расширенный поиск$")
    public void зашлиВРасширенныйПоиск() throws Throwable
    {
        MarkP.AllSet();
    }

    @И("^Выбрали производителя \"([^\"]*)\"$")
    public void выбралиПроизводителя(String prizv) throws Throwable
    {
        MarkP.Firma(prizv);
    }

    @И("^Выбрали цену от \"([^\"]*)\" до \"([^\"]*)\"$")
    public void выбралиЦенуОтДо(String from, String to) throws Throwable
    {
        MarkP.Price(from,to);
    }

    @И("^Применили фильтры$")
    public void применилиФильтры() throws Throwable
    {
        MarkP.EnterFilter();
    }

    @И("^Запомнили \"([^\"]*)\" товар$")
    public void запомнилиТовар(String num) throws Throwable
    {
        MarkP.Save(num);
    }

    @И("^На странице отобразилось \"([^\"]*)\" элементов$")
    public void наСтраницеОтобразилосьЭлементов(String count) throws Throwable
    {
        MarkP.Count(count);
    }

    @Тогда("^Увидели страницу с сохраненным именем товара$")
    public void увиделиСтраницуССохраненнымИменемТовара() throws Throwable
    {
        MarkP.Assert();
    }

    @И("^В строку поиска ввели и нашли \"([^\"]*)\" значение$")
    public void вСтрокуПоискаВвелиИНашлиЗначение(String value) throws Throwable
    {
        MarkP.Search(value);
    }

    @Тогда("^Посчитали количество доступных торговых площадок в разделе «Предложения Магазинов»$")
    public void посчиталиКоличествоДоступныхТорговыхПлощадокВРазделеПредложенияМагазинов() throws Throwable
    {
        MarkP.MarkCount();
    }

}
