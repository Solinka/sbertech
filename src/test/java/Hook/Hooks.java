package Hook;

import Base.Base;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.imageio.ImageIO;

public class Hooks extends Base
{
    public static String currentPage = "";

    @Before
    public void startBrowser()
    {
        System.setProperty("webdriver.chrome.driver", "./webdrivers/chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "./webdrivers/geckodriver.exe");
    }

    @After
    public void afterScenario(Scenario scenario) throws IOException, AWTException
    {
        if (scenario.isFailed())
        {
            BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
            Date dateNow = new Date();
            SimpleDateFormat formatForDateNow = new SimpleDateFormat("dd_mm_yyyy hh_mm_ss ");
            ImageIO.write(image, "png", new File("./Screenshots/" + formatForDateNow.format(dateNow) + scenario.getName() + ".png"));
        }
        Base.driver.quit();
    }


    public static WebElement Element(String XPath) {
        return driver.findElement(By.xpath(XPath));
    }

    public static List<WebElement> Elements(String XPath) {
        return driver.findElements(By.xpath(XPath));
    }

    public static boolean Visible(String XPath)
    {
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        try
        {
            Element(XPath);
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            return true;
        }
        catch (Exception e)
        {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            return false;
        }
    }



    public static void waitVisible(WebElement element, int timeout)
    {
        WebDriverWait wait = new WebDriverWait(Hooks.driver, timeout);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public static void waitClickable(WebElement element, int timeout)
    {
        WebDriverWait wait = new WebDriverWait(Hooks.driver, timeout);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public static void waitinvis(String locator, int timeout)
    {
        WebDriverWait wait = new WebDriverWait(Hooks.driver, timeout);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(locator)));
    }
}