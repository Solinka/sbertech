package Elements;

import Hook.Hooks;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class MarketPage extends Hooks
{
    static public String namezs = "";
    WebDriver driver;
    public MarketPage(WebDriver driver)
    {
        driver = this.driver;
        PageFactory.initElements(driver, this);
    }

    // Показать всех производителей
//    @FindBy(xpath = "//a[contains(@data-reactid, '152')]")
//    public WebElement all;

    // Поле поиска производителя
//    @FindBy(xpath = "//input[contains(@id, '7893318-suggester')]")
//    public WebElement search;
    /*


    FindBy - byee  :D


    */

    public void GoToSection(String name) throws InterruptedException
    {
        waitVisible(Element("//a[contains(@class, 'topmenu__link') and text() = '"+name+"'] | //a[contains(@class, 'horizontal-category') and text() = '"+name+"']"), 10);;
        Element("//a[contains(@class, 'topmenu__link') and text() = '"+name+"'] | //a[contains(@class, 'horizontal-category') and text() = '"+name+"']").click();
    }

    public void GoToSubSection(String name) throws InterruptedException
    {
//        Hooks.waitClickable(Hooks.driver.findElement(By.xpath("//a[contains(@class, 'catalog-menu__list-item') and text() = '"+name+"']")), 15);

        Element("//a[contains(@class, 'catalog-menu__list-item') and text() = '"+name+"']").click();
    }

    public  void AllSet()
    {
        Element(AllSet).click();
    }

    public void Firma(String prod) throws InterruptedException
    {
        waitClickable(Element(AllFirm), 15);
        if (!Visible(SearchFirm))
        {
            Element(AllFirm).click();   //показать всех производителей
        }

        waitClickable(Element(SearchFirm), 15);
        Element(SearchFirm).clear();    //Очищаем поле ввода

        Element(SearchFirm).sendKeys(prod);    //Вводим нужного производителя
        Thread.sleep(1000);
        Element("//div[@data-filter-id = '7893318']//label[text()='"+prod+"']//parent::span").click();  //Клик на чекбокс нужного производителя
    }

    public void Price(String from, String to)
    {
        Element(ProceFrom).sendKeys(from);
//        Element("//h1").click();
        Element(ProceTo).sendKeys(to);
//        Element("//h1").click();
    }

    public void EnterFilter()
    {
        Element(FiltrEnter).click();  //Применить фильтры
    }

    public void Save(String num)
    {
        waitVisible(Element(ElementName), 15);
        namezs = Element("(" + ElementName + ")["+num+"]").getText();
    }

    public void Count(String num)
    {
        waitVisible(Element(ElementName), 15);
        Assert.assertEquals("Не верное количество элементов", Integer.parseInt(num), Elements(ElementName).size());
    }

    public void MarkCount()
    {
        waitVisible(Element(Markets), 15);
        System.out.println("У этого товара -"+Elements(Markets).size()+"- предложения магазинов");
    }

    public void Search(String num)
    {
        if (num.contains("Запомненное"))
        {
            waitVisible(Element(Search), 15);
            Element(Search).sendKeys(namezs);
        }
        Element(Enter).click();
    }

    public void Assert()
    {
        try
        {
            for (int i = 0; i < 10; i++)
                if (Element(Rec).isDisplayed())
                {
                    Element(Enter).click();
                }
        }
        catch (Exception e)
        {}
        waitVisible(Element(ElementNameFromPage), 15);
        Assert.assertTrue("Не совпадает с элементом, котрый запомили или яндекс не открыл нужную страницу",
                            Element(ElementNameFromPage).getText().contains(namezs));
    }

    //Кнопка Перейти ко всем фильтрам
    String AllSet = "//a[contains(text(), 'ко всем фильтрам')] | //span[text() = 'Все фильтры']/../..//button";

    //Показать все фирмы
    String AllFirm = "//span[text() = 'Производитель']/../../..//button";

    //Поле ввода для поиска производителей
    String SearchFirm = "//span[text() = 'Производитель']/../../..//input[@class = 'input__control']";

    //Если не попал на страницу товара
    String Rec = "//div[contains(text(), 'результатов в одной категории')]";

    //Поле ввода цена от
    String ProceFrom = "//span[contains(text(), 'Цена')]/../../..//span[@sign-title = 'от']//input";

    //Поле ввода цена от
    String ProceTo = "//span[contains(text(), 'Цена')]/../../..//span[@sign-title = 'до']//input";

    //Применить фильтры
    String FiltrEnter = "//div[contains(@class, 'button-bar')]//a[contains(@class, 'filtered')]";

    //Имя товара в списке товаров
    String ElementName = "//div[contains(@class, 'title')]//a";

    //Имя товара на странице товара
    String ElementNameFromPage = "//h1";

    //Предложения магазинов
    String Markets = "//a[contains(@class, 'n-product-top')]";

    //Стррока поиска
    String Search = "//input[@id = 'header-search']";

    //Найти кнопка
    String Enter = "//span[contains(@class, 'search')]//button";

}
