package Elements;

import Hook.Hooks;

public class MainPage extends Hooks
{
    public void GoTo(String name) throws InterruptedException
    {
        waitClickable(Element("//div[contains(@class, 'home-arrow__tabs')]//a[text() = '"+name+"']"), 15);
        Element("//div[contains(@class, 'home-arrow__tabs')]//a[text() = '"+name+"']").click();
    }
}
